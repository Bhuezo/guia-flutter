import 'package:flutter/material.dart';
import 'package:guiacurso/ejercicio1/mainEje1.dart';
import 'package:guiacurso/ejercicio2/mainEje2.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
      routes: <String, WidgetBuilder>{
        '/p1': (BuildContext context) => MainEje1('Ejercicio1'),
        '/p2': (BuildContext context) => MainEje2('Ejercicio2'),
      },
    );
  }
}

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Home();
  }
}

class _Home extends State<Home> {
  var list = [
    {
      'icon': Icons.sync,
      'name': 'Ejercicio 1',
      'description':
          'Ejercicio que permite pasar de Celsius a Kelvin y viceversa.',
      'router': '/p1',
    },
    {
      'icon': Icons.sync,
      'name': 'Ejercicio 2',
      'description':
          'Ejercicio que hacer una elección al azar de los datos antes ingresados.',
      'router': '/p2',
    },
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('Hello World!!');
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              var item = list[index];
              return Container(
                margin: EdgeInsets.only(
                  top: 2,
                  left: 15,
                  right: 15,
                ),
                padding: EdgeInsets.all(5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    InkWell(
                      onTap: (){
                        Navigator.pushNamed(context, item['router']);
                      },
                      child: Icon(
                        item['icon'],
                        color: Colors.grey,
                        size: 30,
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(width: 1, color: Colors.grey[300]),
                        ),
                      ),
                      width: MediaQuery.of(context).size.width - 30 - 30 - 15,
                      padding: EdgeInsets.only(
                        top: 5,
                        bottom: 10,
                        left: 5,
                        right: 5,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            item['name'],
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w800,
                                color: Colors.black),
                          ),
                          Text(
                            item['description'],
                            style: TextStyle(
                                fontSize: 11, color: Colors.grey[400]),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          )),
    );
  }
}
