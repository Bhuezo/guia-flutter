import 'package:flutter/material.dart';
import 'package:guiacurso/ejercicio2/controller/controller.dart';

class ViewEje2 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ViewEje2();
  }
}

class _ViewEje2 extends State<ViewEje2> {
  var candidato = TextEditingController();
  var items = [];
  var data = false;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.all(15),
      height: 300,
      child: Column(
        children: <Widget>[
          Text(
            'Meter nombre de candidato y elegira uno al azar ',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w800),
          ),
          TextField(
            controller: candidato,
            decoration: InputDecoration(labelText: 'Candidato'),
          ),
          RaisedButton(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[Icon(Icons.file_upload), Text('Ingresar')],
            ),
            onPressed: () {
              setState(() {
                if (candidato.text.trim() != '') {
                  items.add(candidato.text);
                  if (data == false && items.length > 1) {
                    data = true;
                  }
                } else {
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text('Valor vacío'),
                    action: SnackBarAction(
                      label: 'Cerrar',
                      onPressed: () {},
                    ),
                  ));
                }
              });
            },
          ),
          Row(
            children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width / 2 - 30,
              child: Column(
            children: <Widget>[
              for (var i = 0; i < items.length; i++)
                Text('${i + 1} -' + items[i]),
            ],
          )),
          if (data == true)
            Container(
              width: MediaQuery.of(context).size.width / 2 - 30,
              child: Column(
              children: <Widget>[
                FloatingActionButton(
                  tooltip: 'Calcular',
                  child: Icon(Icons.equalizer),
                  onPressed: () {
                    var data = ControllerEje2().calcular(items);
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text('$data'),
                      action: SnackBarAction(
                        label: 'Cerrar',
                        onPressed: () {},
                      ),
                    ));
                  },
                ),
              ],
            )),
            ],
          )
        ],
      ),
    );
  }
}
