import 'package:flutter/material.dart';
import 'package:guiacurso/ejercicio1/mainEje1.dart';
import 'package:guiacurso/ejercicio2/view/view.dart';

class MainEje2 extends StatelessWidget {
  String title;

  MainEje2(this.title);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('$title'),
      ),
      body: ViewEje2(),

    );
  }
  
}