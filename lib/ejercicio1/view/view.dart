import 'package:flutter/material.dart';
import 'package:guiacurso/ejercicio1/controller/controller.dart';

class ViewEje1 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ViewEje1();
  }
}

class _ViewEje1 extends State<ViewEje1> {
  final data1 = TextEditingController();
  final data2 = TextEditingController();
  String result = 'N/A';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        padding: EdgeInsets.only(
          top: 30,
          left: 20,
          right: 20,
        ),
        child: Column(
          children: <Widget>[
            Text(
              'Temperature Converter',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            ),
            TextField(
              controller: data1,
              decoration: InputDecoration(
                labelText: 'Celsius',
              ),
            ),
            TextField(
              controller: data2,
              decoration: InputDecoration(
                labelText: 'Kelvin',
              ),
            ),
            RaisedButton(
              color: Colors.blue,
              textColor: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.sync),
                  Text('Convert to Kelvin'),
                ],
              ),
              onPressed: () {
                setState(() {
                  if (data1.text.trim() == '') {
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text('Invalid Value'),
                      action: SnackBarAction(
                        label: 'Undo',
                        onPressed: () {
                          // Some code to undo the change.
                        },
                      ),
                    ));
                  } else {
                    result = ControllerEje1().toKelvin(double.parse(data1.text));
                    data2.text = result;
                  }
                });
              },
            ),
            RaisedButton(
              color: Colors.blue,
              textColor: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.sync),
                  Text('Convert to Celsius'),
                ],
              ),
              onPressed: () {
                setState(() {
                  if (data2.text.trim() == '') {
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text('Invalid Value'),
                      action: SnackBarAction(
                        label: 'Undo',
                        onPressed: () {
                          // Some code to undo the change.
                        },
                      ),
                    ));
                  } else {
                    result = ControllerEje1().toCelsius(double.parse(data2.text));
                    data1.text = result;
                  }
                });
              },
            ),
          ],
        ));
  }
}
