import 'package:flutter/material.dart';
import 'package:guiacurso/ejercicio1/view/view.dart';

class MainEje1 extends StatelessWidget {
  String title;
  MainEje1(this.title);

  @override
  

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$title'),
      ),
      body: ViewEje1(),
    );
  }
  
}